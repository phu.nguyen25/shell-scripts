#!/bin/sh

# -----------------------------------------

if ! [ "$1" ] || ! [ "$2" ]; then
  echo "Usage: $0 {domain} {root-dir}"
  exit 1 ;
fi

# -----------------------------------------

function remove_host {
	echo "Removing files and root folder ..."
	rm -r "/etc/nginx/sites-available/$1" "/etc/nginx/sites-enabled/$1" "$2"
	echo "Nginx checking configuration..."
	if nginx -t && echo "Restarting Nginx server ..." && systemctl restart nginx; then
	  echo ""
	  echo "Finish removing host $1 with root folder in $2"
	else
	  echo ""
	  echo "Fail to create host for $1. Configuration error."
	fi
	echo ""
}

# -----------------------------------------

echo "These files and folders will be removed:"
echo "/etc/nginx/sites-available/$1"
echo "/etc/nginx/sites-enabled/$1"
echo "$2"

read -p "Please comfirm that you want to remove above files and folders (yes/no) ? " choice
case "$choice" in
  yes ) remove_host $1 $2;;
  * ) exit 1;;
esac
